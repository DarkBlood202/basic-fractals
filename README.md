# Basic Fractals

Using simple (and slow) PyGame, Pillow and SDL2 code to generate images of 2D fractals.

<img src="./767.png" width="50%"/>

Python file dependencies: <a href="http://pygame.org">PyGame</a>, and for one file, Pillow

C compilation instructions (replace file name as needed): `gcc -std=c89 -Wextra -pedantic-errors -o main.out main.c -lSDL2 -lm`
