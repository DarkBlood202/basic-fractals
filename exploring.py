import pygame

from random import randint
import time

X, Y = 240, 240
skrin = pygame.display.set_mode((X, Y), 0, 0)

lari = True

def f(z):
    #return 0.75*z + 0.25/z**3
    #return z - (4 * z ** 3 - 2) / (12 * z ** 2)
    return 2/3*z + 1/3/z/z

x_center = 0.0
y_center = 0.0
x_domain = 4.0
y_domain = 4.0

max_applications_of_f = 8

while lari:
    keys = pygame.key.get_pressed()

    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            lari = False
        if e.type == pygame.KEYDOWN:
            if e.key == pygame.K_s:
                pygame.image.save(skrin,"./"+str(randint(100,999))+'.png')

            if e.key == pygame.K_UP:
                y_center += y_domain/4
            if e.key == pygame.K_DOWN:
                y_center -= y_domain/4
            if e.key == pygame.K_RIGHT:
                x_center += y_domain/4
            if e.key == pygame.K_LEFT:
                x_center -= y_domain/4

            if e.key == pygame.K_SPACE:
                x_domain /= 2
                y_domain /= 2
            if e.key == pygame.K_LSHIFT:
                x_domain *= 2
                y_domain *= 2

            if e.key == pygame.K_EQUALS:
                max_applications_of_f += 1
            if e.key == pygame.K_MINUS:
                if max_applications_of_f > 0: max_applications_of_f -= 1

            skrin.fill((0,0,0))

            start_time = time.time()

            cbrt2 = 2 ** (1/3)
            z_1 = complex(1/cbrt2)
            z_2 = complex(-1/2/cbrt2 - pow(3/cbrt2**2,0.5)/2 *1j)
            z_3 = complex(-1/2/cbrt2 + pow(3/cbrt2**2,0.5)/2 *1j)

            for a in range(X):
                for b in range(Y):
                    x = (a-X/2)/(X/x_domain) + x_center
                    y = (b-Y/2)/(Y/y_domain) - y_center
                    c = complex(x-y*1j)
                    terkurang = 10
                    i = 0
                    while i < max_applications_of_f and terkurang > 0.05:
                        try: c = f(c); i+=1
                        except ZeroDivisionError: i=max_applications_of_f; break
                        d1 = (c-z_1) * (c-z_1).conjugate()
                        d2 = (c-z_2) * (c-z_2).conjugate()
                        d3 = (c-z_3) * (c-z_3).conjugate()
                        d1, d2, d3 = [d.real for d in [d1,d2,d3]]
                        terkurang = min(d1, d2, d3)

                    if   d1 == terkurang: warna = (255, 0,   0)
                    elif d2 == terkurang: warna = (0,   255, 0)
                    elif d3 == terkurang: warna = (0,   0,   255)
                    warna = [int(min(255, w*i/max_applications_of_f)) for w in warna]
                    skrin.set_at((a,b), warna)

            pygame.display.flip()

            print(round(time.time() - start_time, 2), 'seconds')

pygame.quit()
quit()
