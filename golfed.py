import pygame

WIDTH, HEIGHT = 480, 480
screen = pygame.display.set_mode((WIDTH, HEIGHT), 0, 0)

def f(z):
    if not z == 0:
        # Root convergence algorithm: z-f(z)/f'(z)
        return z - (z**4 - 1) / (4 * z**3)
    else:
        return complex(0+0j)

# Domain of the graph for plotting
x_domain = 2.0
y_domain = 2.0j  # j is the imaginary unit

for number_of_times_to_apply_f in range(1,11):
    for x in range(WIDTH):
        for y in range(HEIGHT):

            # Scaling from pixels of screen to complex number on graph
            z = complex((2 * x/WIDTH - 1)*x_domain - (2 * y/HEIGHT - 1)*y_domain)

            # Apply root-finding algorithm a few times
            for _ in range(number_of_times_to_apply_f):
                z = f(z)

            # Find the distance of new_z to each root
            d1 = abs(z - 1 )  # 1 is a root
            d2 = abs(z - 1j)  # i is a root
            d3 = abs(z + 1 )  # -1 is a root
            d4 = abs(z + 1j)  # -i is a root

            the_smallest_distance_to_a_root = min(d1, d2, d3, d4)

            if d1 is the_smallest_distance_to_a_root:
                color = (255, 0, 0)  # red

            elif d2 is the_smallest_distance_to_a_root:
                color = (0, 255, 0)  # green

            elif d3 is the_smallest_distance_to_a_root:
                color = (0, 0, 255)  # blue

            elif d4 is the_smallest_distance_to_a_root:
                color = (255, 255, 0)  # yellow

            screen.set_at((x, y), color)

    pygame.display.update()

    pygame.image.save(screen, "./"+str(number_of_times_to_apply_f)+".png")

while True:
    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            pygame.quit()
            break
