#include <stdlib.h>

#include <SDL2/SDL.h>

#include <math.h>
#include <complex.h>

#define WINDOW_WIDTH 360
#define MAX_APPLICATIONS 2
#define E 1e-3

float complex f(float q, float complex z) {
    return (1.0-q)*z + q/(z*z*z);
}

int main(void) {
    SDL_Event event;
    SDL_Renderer *renderer;
    SDL_Window *window;

    int x; int y;
    float complex z;

    int i; short cond; float q;
    short c1, c2, c3;
    float d1, d2, d3, d4;
    c1 = 0; c2 = 0; c3 = 0;


    SDL_Init(SDL_INIT_VIDEO);
    SDL_CreateWindowAndRenderer(WINDOW_WIDTH, WINDOW_WIDTH, 0, &window, &renderer);

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);

    for (q = 0.0; q < 1.0; q = q + 0.002) {
        for (x = 0; x < WINDOW_WIDTH; ++x) {
            for (y = 0; y < WINDOW_WIDTH; ++y) {
                z = (2.0*x/WINDOW_WIDTH - 1.0) * 2 + (2.0*y/WINDOW_WIDTH - 1.0) * 2 * I;
                i = 0;
                d1 = 1.0; d2 = 1.0; d3 = 1.0; d4 = 1.0;
                while (i < MAX_APPLICATIONS && d1 > E && d2 > E && d3 > E && d4 > E ) {
                    if ( x == WINDOW_WIDTH/2 && y == WINDOW_WIDTH/2) {
                        i = MAX_APPLICATIONS;
                        break;
                    } else {
                        z = f(q,z);
                        ++i;
                    }
                    d1 = creal((z-1.0) * conj(z-1.0));
                    d2 = creal((z - I) * conj(z - I));
                    d3 = creal((z+1.0) * conj(z+1.0));
                    d4 = creal((z + I) * conj(z + I));
                }

                if (d1 < d2 && d1 < d3 && d1 < d4) {c1 = 255*i/MAX_APPLICATIONS; c2 = 0; c3 = 0;}
                if (d2 < d1 && d2 < d3 && d2 < d4) {c1 = 0; c2 = 255*i/MAX_APPLICATIONS; c3 = 0;}
                if (d3 < d1 && d3 < d2 && d3 < d4) {c1 = 0; c2 = 0; c3 = 255*i/MAX_APPLICATIONS;}
                if (d4 < d1 && d4 < d2 && d4 < d3) {c1 = 255*i/MAX_APPLICATIONS; c2 = 255*i/MAX_APPLICATIONS; c3 = 0;}
                SDL_SetRenderDrawColor(renderer, c1, c2, c3, 255);
                SDL_RenderDrawPoint(renderer, x, y);
            }
        }
        SDL_RenderPresent(renderer);
    }

    while (1) {
        if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
            break;
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return EXIT_SUCCESS;
}
