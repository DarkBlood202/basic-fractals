#include <stdlib.h>

#include <SDL2/SDL.h>

#include <math.h>
#include <complex.h>

#define WINDOW_WIDTH 240
#define MAX_APPLICATIONS 7
#define E 1e-2

float complex f(float q, float complex z) {
    return (1.0-q)*z + q/(z*z*z*z);
}

int main(void) {
    SDL_Event event;
    SDL_Renderer *renderer;
    SDL_Window *window;

    int x; int y;
    float complex z;
    float x_dom, y_dom;

    int i; short cond; float q;
    short c1, c2, c3;
    float d1, d2, d3, d4, d5, d6;
    float complex z1, z2, z3, z4, z5, z6;

    z1 = 1.0 + 0.0 * I;
    z2 = 0.30901699437494745 + 0.9510565162951535 * I;
    z3 = -0.8090169943749473 + 0.5877852522924732 * I;
    z4 = -0.8090169943749473 - 0.5877852522924732 * I;
    z5 = 0.30901699437494745 - 0.9510565162951535 * I;
    z6 = 1.0 + 0.0 * I;


    SDL_Init(SDL_INIT_VIDEO);
    SDL_CreateWindowAndRenderer(WINDOW_WIDTH, WINDOW_WIDTH, 0, &window, &renderer);

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);

    for (q = 0.0; q < 1.0; q = q + 1.0/600) {
        x_dom = q * q * 57 + 4.0/48;
        y_dom = q * q * 57 + 4.0/48;
        if (1 || 2.0/48 < q <= 2.4/48 || 23.8/48 < q < 24.2/48 || q >= 23.0/24)
        for (x = 0; x < WINDOW_WIDTH; ++x) {
            for (y = 0; y < WINDOW_WIDTH; ++y) {
                z = (2.0*x/WINDOW_WIDTH - 1.0) * x_dom + (2.0*y/WINDOW_WIDTH - 1.0) * y_dom * I;
                i = 0;
                c1 = 0; c2 = 0; c3 = 0;
                d1 = 1.0; d2 = 1.0; d3 = 1.0; d4 = 1.0; d5 = 1.0; d6 = 1.0;
                while (i < MAX_APPLICATIONS && d1 > E && d2 > E && d3 > E && d4 > E && d5 > E && d6 > E ) {
                    if ( x == WINDOW_WIDTH/2 && y == WINDOW_WIDTH/2) {
                        i = MAX_APPLICATIONS;
                        break;
                    } else {
                        z = f(q,z);
                        ++i;
                    }
                    d1 = creal((z-z1) * conj(z-z1));
                    d2 = creal((z-z2) * conj(z-z2));
                    d3 = creal((z-z3) * conj(z-z3));
                    d4 = creal((z-z4) * conj(z-z4));
                    d5 = creal((z-z5) * conj(z-z5));
                    /*d6 = creal((z-z6) * conj(z-z6));*/
                }

                if (d1 < d2 && d1 < d3 && d1 < d4 && d1 < d5 && d1 < d6) {c1 = 255*i/MAX_APPLICATIONS; c2 = 230*i/MAX_APPLICATIONS; c3 = 0;}
                if (d2 < d3 && d2 < d1 && d2 < d4 && d2 < d5 && d2 < d6) {c1 = 255*i/MAX_APPLICATIONS; c2 = 0; c3 = 0;}
                if (d3 < d2 && d3 < d1 && d3 < d4 && d3 < d5 && d3 < d6) {c1 = 255*i/MAX_APPLICATIONS; c2 = 230*i/MAX_APPLICATIONS; c3 = 0;}
                if (d4 < d2 && d4 < d3 && d4 < d1 && d4 < d5 && d4 < d6) {c1 = 255*i/MAX_APPLICATIONS; c2 = 230*i/MAX_APPLICATIONS; c3 = 0;}
                if (d5 < d2 && d5 < d3 && d5 < d4 && d5 < d1 && d5 < d6) {c1 = 0; c2 = 180*i/MAX_APPLICATIONS; c3 = 0;}
                /*if (d6 < d2 && d6 < d3 && d6 < d4 && d6 < d5 && d6 < d1) {c1 = 255*i/MAX_APPLICATIONS; c2 = 230*i/MAX_APPLICATIONS; c3 = 0;}*/
                SDL_SetRenderDrawColor(renderer, c1, c2, c3, 255);
                SDL_RenderDrawPoint(renderer, y, x);
            }
        }
        SDL_RenderPresent(renderer);
    }

    while (1) {
        if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
            break;
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return EXIT_SUCCESS;
}
