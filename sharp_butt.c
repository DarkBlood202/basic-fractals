#include <stdlib.h>
#include <stdio.h>

#include <SDL2/SDL.h>

#include <math.h>
#include <complex.h>

#define WINDOW_WIDTH 600
#define MAX_APPLICATIONS 2
#define E 1e-3

float complex f(float complex old_z, float complex c) {
    return old_z*old_z + c;
}

int main(void) {
    SDL_Event event;
    SDL_Renderer *renderer;
    SDL_Window *window;

    int x; int y;
    float complex z;
    float complex new_z;
    float d;

    int i; int max_f_apps;
    short c1, c2, c3;
    c1 = 0; c2 = 0; c3 = 0;


    SDL_Init(SDL_INIT_VIDEO);
    SDL_CreateWindowAndRenderer(WINDOW_WIDTH, WINDOW_WIDTH, 0, &window, &renderer);

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);

    for (max_f_apps = 1; max_f_apps < 30; ++max_f_apps) {
        for (x = 0; x < WINDOW_WIDTH; ++x) {
            for (y = 0; y < WINDOW_WIDTH; ++y) {

                z = (x-460)/250.0 + (y-300)/250.0 * I;
                new_z = z;

                c1 = rand()%255; c2 = rand()%255; c3 = rand()%255;

                for (i=0; i<max_f_apps; i++) new_z = f(new_z, z);

                d = creal(new_z * conj(new_z));

                if (d > 10.0) {c1 = 255; c2 = 255; c3 = 255;}

                SDL_SetRenderDrawColor(renderer, c1, c2, c3, 255);
                SDL_RenderDrawPoint(renderer, x, y);
            }
        }
        SDL_RenderPresent(renderer);
    }
    printf("Finish!\n");

    while (1) {
        if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
            break;
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return EXIT_SUCCESS;
}
