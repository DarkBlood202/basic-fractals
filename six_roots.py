import pygame

from math import sin, cos, radians
import time
from random import randint

X, Y = 1080, 1080
skrin = pygame.display.set_mode((X, Y), 0, 0)

lari = True

def f(z): return 5/6*z + z**-5/6

skrin.fill((255, 255, 255))



z_1 = complex(cos(radians(0)) + sin(radians(0)) * 1j)
z_2 = complex(cos(radians(60)) + sin(radians(60)) * 1j)
z_3 = complex(cos(radians(120)) + sin(radians(120)) * 1j)
z_4 = complex(cos(radians(180)) + sin(radians(180)) * 1j)
z_5 = complex(cos(radians(240)) + sin(radians(240)) * 1j)
z_6 = complex(cos(radians(300)) + sin(radians(300)) * 1j)

for max_f_applications in range(8, 9):
    start_time = time.time()

    for a in range(X):
        for b in range(Y):
            x = (2*a/X - 1) * 1.5
            y = (2*b/Y - 1) * 1.5
            c = complex(x + y*1j)
            minimum = 10
            i = 2
            warna = (0, 0, 0)
            while i < max_f_applications and minimum > 1e-3 and abs(c.real) < 1e8 and abs(c.imag) < 1e8:
                try: c = f(c); i+=1
                except ZeroDivisionError: i=max_f_applications; break
                d1 = (c-z_1) * (c-z_1).conjugate()
                d2 = (c-z_2) * (c-z_2).conjugate()
                d3 = (c-z_3) * (c-z_3).conjugate()
                d4 = (c-z_4) * (c-z_4).conjugate()
                d5 = (c-z_5) * (c-z_5).conjugate()
                d6 = (c-z_6) * (c-z_6).conjugate()

                d1, d2, d3, d4, d5, d6 = [d.real for d in [d1,d2,d3,d4,d5,d6]]
                minimum = min(d1, d2, d3, d4, d5, d6)

            if   d1 == minimum: warna = (255, 160, 0)
            elif d2 == minimum: warna = (255, 0, 160)
            elif d3 == minimum: warna = (160, 0, 255)
            elif d4 == minimum: warna = (0, 160, 255)
            elif d5 == minimum: warna = (0, 255, 160)
            elif d6 == minimum: warna = (160, 255, 0)
            warna = [int(w*i/max_f_applications) for w in warna]
            skrin.set_at((a,b), warna)

    pygame.display.flip()

    print('max',max_f_applications, 'applications:', round(time.time() - start_time, 1))

    pygame.image.save(skrin,"./2160p_" + str(max_f_applications) + "applications.png")

while lari:
    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            lari = False
        if e.type == pygame.KEYDOWN:
            if e.key == pygame.K_s:
                pygame.image.save(skrin,str(randint(100,999))+".png")

pygame.quit()
quit()
