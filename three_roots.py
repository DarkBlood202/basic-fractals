import pygame

from random import randint
import time

X, Y = 720, 720
skrin = pygame.display.set_mode((X, Y), 0, 0)

lari = True

def f(z):
    return z - (4 * z ** 3 - 2) / (12 * z ** 2)

skrin.fill((0,0,0))

start_time = time.time()

cbrt2 = 2 ** (1/3)
z_1 = complex(1/cbrt2)
z_2 = complex(-1/2/cbrt2 - pow(3/cbrt2**2,0.5)/2 *1j)
z_3 = complex(-1/2/cbrt2 + pow(3/cbrt2**2,0.5)/2 *1j)

for a in range(X):
    for b in range(Y):
        x = 4*(a/X - 0.5)
        y = 4*(b/Y - 0.5)
        c = complex(x+y*1j)
        terkurang = 10
        i = 0
        while i < 10 and terkurang > 0.00000001:
            try: c = f(c); i+=1
            except ZeroDivisionError: w=255; i=10; break
            d1 = (c-z_1) * (c-z_1).conjugate()
            d2 = (c-z_2) * (c-z_2).conjugate()
            d3 = (c-z_3) * (c-z_3).conjugate()
            d1, d2, d3 = [d.real for d in [d1,d2,d3]]
            terkurang = min(d1, d2, d3)

        if   d1 == terkurang: warna = (255, 0,   0)
        elif d2 == terkurang: warna = (0,   255, 0)
        elif d3 == terkurang: warna = (0,   0,   255)
        warna = [int(min(255, w*i/10)) for w in warna]
        skrin.set_at((a,b), warna)

pygame.display.flip()

print(round(time.time() - start_time, 1))

while lari:
    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            lari = False
        if e.type == pygame.KEYDOWN:
            if e.key == pygame.K_s:
                pygame.image.save(skrin,"./"+str(randint(100,999))+'.png')

pygame.quit()
quit()
