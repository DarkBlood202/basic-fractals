from PIL import Image, ImageDraw

from math import sin, cos, radians
import time

X, Y = 720,720

img = Image.new('RGB', (X,Y), (0, 0, 255))
draw = ImageDraw.Draw(img)

def f(z): return 5/6*z + z**-5/6

z_1 = complex(sin(radians(0)) + cos(radians(0)) * 1j)
z_2 = complex(sin(radians(60)) + cos(radians(60)) * 1j)
z_3 = complex(sin(radians(120)) + cos(radians(120)) * 1j)
z_4 = complex(sin(radians(180)) + cos(radians(180)) * 1j)
z_5 = complex(sin(radians(240)) + cos(radians(240)) * 1j)
z_6 = complex(sin(radians(300)) + cos(radians(300)) * 1j)

start_time = time.time()

for a in range(X):
    for b in range(Y):
        x = (2*a/X - 1) * 3.5
        y = (2*b/Y - 1) * 3.5
        c = complex(x + y*1j)
        minimum = 10
        i = 0
        warna = (0, 0, 0)
        while i < 7 and minimum > 1e-20 and abs(c.real) < 1e8 and abs(c.imag) < 1e8:
            try: c = f(c); i+=1
            except ZeroDivisionError: i=7; break
            d1 = (c-z_1) * (c-z_1).conjugate()
            d2 = (c-z_2) * (c-z_2).conjugate()
            d3 = (c-z_3) * (c-z_3).conjugate()
            d4 = (c-z_4) * (c-z_4).conjugate()
            d5 = (c-z_5) * (c-z_5).conjugate()
            d6 = (c-z_6) * (c-z_6).conjugate()

            d1, d2, d3, d4, d5, d6 = [d.real for d in [d1,d2,d3,d4,d5,d6]]
            minimum = min(d1, d2, d3, d4, d5, d6)

        if   d1 == minimum: warna = (255, 160, 0)
        elif d2 == minimum: warna = (255, 0, 160)
        elif d3 == minimum: warna = (160, 0, 255)
        elif d4 == minimum: warna = (0, 160, 255)
        elif d5 == minimum: warna = (0, 255, 160)
        elif d6 == minimum: warna = (160, 255, 0)
        warna = [int(w*i/7) for w in warna]
        draw.point((a,b), tuple(warna))

print('max',7, 'applications:', round(time.time() - start_time, 1))

img.show()
